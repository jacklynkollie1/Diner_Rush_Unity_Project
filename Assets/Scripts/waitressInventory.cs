using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waitressInventory : MonoBehaviour
{
    public int itemheld = -1;

    public GameObject[] itemIcons;

    public Cook cook;


    public void UpdateItemHeldIcon()
    {
        for (int i = 0; i < itemIcons.Length; i++)
        {
            if (i == itemheld)
            {
                itemIcons[i].SetActive(true);
            }
            else
            {
                itemIcons[i].SetActive(false);
            }
        }
    }


    public void GiveItemToChef() // cook button
    {
        // find an empty slot in chef's inventory
        for (int i = 0; i < cook.inventory.Length; i++)
        {
            // if waitress is holding nothing, break out of loop
            if (itemheld == -1)
            {
                Debug.Log("Waitress is not carrying anything.");
                break;
            }

            if (cook.inventory[i] == -1)
            {
                // give item to chef
                cook.inventory[i] = itemheld;
                // blank what waitress is holding
                Debug.Log("Waitress gave item to chef: " + itemheld);
                itemheld = -1;
                UpdateItemHeldIcon();
                cook.EvaluateInventory();
                break;
            }

        }
        

    }


}
