using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerHandler : MonoBehaviour
{
    public float gameTimer = 120.0f;

    public Transform[] customerPositions;

    public Transform[] customerQuePositions;

    public List<int> availablePositions = new List<int>();

    public List<Customer> customerPool;

    public bool canBeAnUnPayingCustomer;

    public int noOfUnpayingCustomers;

    public int maxNoOfUnpayableCustomers;

    public static CustomerHandler _instance;

    public Text timerText;
   
    public bool timerStopped;

    public GameObject orderPanel;

    
    

    // Start is called before the first frame update
    void Start()
    {
        _instance = this;
        noOfUnpayingCustomers = 0;
        StartCoroutine(BringCustomers());
        timerText.text = "02:00";
        int remainderVal = levelManager.levelNo % 10;
        if (remainderVal > 6 || remainderVal == 0)
        {
            canBeAnUnPayingCustomer = true;
            if (remainderVal == 7)
            {
                maxNoOfUnpayableCustomers = 1;
            }
            else if (remainderVal == 8)
            {
                maxNoOfUnpayableCustomers = Random.Range(1, 3);
            }
            else if (remainderVal == 9)
            {
                maxNoOfUnpayableCustomers = Random.Range(1, 4);
            }
            else
            {
                maxNoOfUnpayableCustomers = Random.Range(3, 5);
            }
        }

    }

    void TimerVaue()
    {
        int timer = Mathf.FloorToInt(gameTimer);
        if(timer < 60)
        {
            if (timer > 9)
                timerText.text = "00:" + timer;
            else timerText.text = "00:" + timer;
        }
       
        else
        {
            if(timer == 120)
            {
                timerText.text = "02:00";
            }

            else
            {
                int remainder = timer % 60;
                if (remainder > 9)
                    timerText.text = "01:" + remainder;
                else
                    timerText.text = "01:0" + remainder;
            }
        }

        
    }

    bool wait = true;
    IEnumerator BringCustomers()
    {
        yield return new WaitForSeconds(5f);
        if (levelManager.levelNo != 1)
        {
                InitializeCustomers();
            
        }
    }


    public int noOfCustomers;

    public void InitializeCustomers()
    {
        int customerNo = Random.Range(0, customerPool.Count);
        int availablePosForCustomer = Random.Range(0, availablePositions.Count);

        noOfCustomers++;
        customerPool[customerNo].MoveToPosition.position = customerPositions[availablePositions[availablePosForCustomer]].position;

        customerPool[customerNo].positionTaken = availablePositions[availablePosForCustomer];


     

        availablePositions.Remove (availablePositions[availablePosForCustomer]);

        customerPool.Remove(customerPool[customerNo]);

    }





    // Update is called once per frame
    void Update()
    {
        
    }



}
