using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dataModifier : MonoBehaviour
{
    [SerializeField] int money;
    [SerializeField]
    Text moneyText;
    Cook cook;
    //public bool hasIngredient = false;
    public int ingredientIndex = -1;

    public int[] upgradeLevels;
    public UpgradeSO ingredient;

    


    private void Start()
    {
        moneyText = GameObject.Find("Money Text").GetComponent<Text>();
        
    }

    public int Money()
    {
        return money;
    }

    public void modifyMoney(int _money)
    {
        money += _money;
        money = Mathf.Clamp(money, 0, 999999);
    }

    void Update()
    {
        moneyText.text = money.ToString();
    }
}