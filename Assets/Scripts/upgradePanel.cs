using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class upgradePanel : MonoBehaviour
{
    public GameObject upgrades;
    

    dataModifier DataModifier;

    void Start()
    {
       DataModifier = GameObject.FindObjectOfType<dataModifier>();
        upgrades.SetActive(false);
    }

   

    public void  ShowPanel(int buttonClicked)
    {
        if (buttonClicked == 0)
        {
            LeanTween.scale(upgrades, new Vector3(1f, 1f, 1f), 0.5f).setEase(LeanTweenType.easeInCirc);
            upgrades.SetActive(true);
        }
        if (buttonClicked == 1)
        {
            LeanTween.scale(upgrades, new Vector3(0.0f, 0.0f, 0.0f), 0.5f).setEase(LeanTweenType.easeOutCubic);
           
            StartCoroutine(ExecuteAfterTime(1));

            IEnumerator ExecuteAfterTime(float time)
            {
                yield return new WaitForSeconds(time);

                upgrades.SetActive(false);

            }
        }
    }

    public void UpdateItemBtn()
    {
        UpgradeItem item = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponentInParent<UpgradeItem>();
        //Debug.Log(item.ingredient.upgradeLevelCost[item.GetUpdateLevel()]);

        if (item && DataModifier.Money() >= item.ingredient.upgradeLevelCost[item.GetUpdateLevel()])
        {
            DataModifier.modifyMoney(item.ingredient.upgradeLevelCost[item.GetUpdateLevel()]);

            
            item.Upgrade(item.GetUpdateLevel());
            item.IncreaseUpdateLevel();
        }
    }
}
