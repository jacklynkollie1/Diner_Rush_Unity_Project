using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelPanel : MonoBehaviour
{
    public GameObject levelpanel;
    // Start is called before the first frame update
    void Start()
    {
        levelpanel.SetActive(false);
    }

    // Update is called once per frame
    
    

        public void showlevelpanel(int buttonClicked)
        {
            if (buttonClicked == 0)
            {
                LeanTween.scale(levelpanel, new Vector3(1f, 1f, 1f), 0.5f).setEase(LeanTweenType.easeInCirc);
                levelpanel.SetActive(true);
            }
            if (buttonClicked == 1)
            {
                LeanTween.scale(levelpanel, new Vector3(0.0f, 0.0f, 0.0f), 0.5f).setEase(LeanTweenType.easeOutCubic);

                StartCoroutine(ExecuteAfterTime(1));

                IEnumerator ExecuteAfterTime(float time)
                {
                    yield return new WaitForSeconds(time);

                    levelpanel.SetActive(false);

                }
            }
        }

}

