using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
    public float customerWaitingTime;

    public int positionTaken;

    public bool orderPlaced;

    public GameObject orderPanel;


    public Transform MoveToPosition;

    public GameObject order;
   

    private Animation anim;

   
    public int orderheld;

    public bool perfect;

    public bool shouldBePerfectIfServed;

    dataModifier dataModifier;

   

    

   

    // Start is called before the first frame update
    void Start()
    {
        MoveToPosition = transform;

        order.SetActive(false);
     
    }

    // Update is called once per frame
    void Update()
    {

        
    }


    

    public void OrderMeal() //ordermeal show button
    {
        Invoke("playorderAnimation", 0.5f); 

        LeanTween.scale(orderPanel, new Vector3(1f, 1f, 1f), 0.5f).setEase(LeanTweenType.easeInCirc);
        Invoke("showproduct", 0.5f);

        Debug.Log(" clicked me ");
    }

    public void GetMeal()
    {

    }


    
    void showproduct()
    {
        LeanTween.scale(order, new Vector3(1f, 1f, 1f), 0.5f).setEase(LeanTweenType.easeInCirc);
        order.SetActive(true);
    }

    public string readMenu = "C1_Read_Menu";
    public void playreadmenuAnim()
    {
        gameObject.GetComponent<Animation>().CrossFade(readMenu);
    }
}
