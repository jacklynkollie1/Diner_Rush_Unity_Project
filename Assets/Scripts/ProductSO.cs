using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ProductAsset", menuName = "ScriptableAssets/ProductAsset", order = 1)]
public class ProductSO : ScriptableObject
{
    public Sprite[] itemIcons;
}
