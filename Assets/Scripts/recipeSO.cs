using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class recipeSO : ScriptableObject
{
    public int[] recipeIngredients;
    public int cookTime;
    public  GameObject product;
}
