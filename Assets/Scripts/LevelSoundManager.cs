using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSoundManager : MonoBehaviour
{

	public static LevelSoundManager _instance;
	

	public AudioSource bell;
	

	public AudioSource customerPay_Coin;

	public AudioSource cooking_Burger;
	

	
	public AudioSource bttn_click;
	public AudioSource bottle_click;
	public AudioSource customerEat;



	public AudioSource successful_level;
	public AudioSource unsuccessful_level;
	public AudioSource eat_random;
	public AudioSource drink;
	public AudioSource grunt;
	public AudioSource coinAdd;
	public AudioSource come_random;

	public AudioSource drink2;
	public AudioSource dustbin;
	public AudioSource Gameoverpanel;

	
	public AudioSource caught;
	void Awake()
	{
		_instance = this;
	}



	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
