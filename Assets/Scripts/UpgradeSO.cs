using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "UpgradeAsset", menuName = "ScriptableAssets/UpgradeAsset", order = 1)]
public class UpgradeSO : ScriptableObject
{
    public int[] upgradeLevelCost;
    public int[] ingredientCostPerLevel;
    public bool[] ingredient;
}
