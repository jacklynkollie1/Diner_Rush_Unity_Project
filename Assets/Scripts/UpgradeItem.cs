using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeItem : MonoBehaviour
{
    public GameObject startImage;
    public GameObject[] upgradeImages;
    public GameObject[] starImages;
    public UpgradeSO ingredient;
    public Text upgradeCostText;
    public Text itemCostText;

    dataModifier dataModifier;
    public int upgradeLevelIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        dataModifier = GameObject.FindObjectOfType<dataModifier>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Upgrade(int level)
    {
        startImage.SetActive(false);
        for (int i = 0; i < upgradeImages.Length; i++)
        {
            if (i == level)
            {
                upgradeImages[i].SetActive(true);
                starImages[i].SetActive(true);
                upgradeCostText.text = ingredient.upgradeLevelCost[i].ToString();
                itemCostText.text = ingredient.ingredientCostPerLevel[i].ToString();
            }
            else upgradeImages[i].SetActive(false);
        }
    }

    public int GetUpdateLevel()
    {
        return dataModifier.upgradeLevels[upgradeLevelIndex];
    }

    public void IncreaseUpdateLevel()
    {
        dataModifier.upgradeLevels[upgradeLevelIndex]++;
        if (dataModifier.upgradeLevels[upgradeLevelIndex] >= upgradeImages.Length) dataModifier.upgradeLevels[upgradeLevelIndex] = upgradeImages.Length - 1;
    }
}
