using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cook : MonoBehaviour
{

    public Image fill;
    //public float fillAmount;

    public GameObject product;

    public GameObject cookMealButton;
    public AudioClip cookAudio;
    
    [SerializeField] private Animation cookingAnimation;

    // what the chef is carrying
    public int[] inventory = new int[5];

    public recipeSO recipe;

    bool isCooking = false;
    public float cookTime = -1f;


    void Start()
    {
        InitialiseInventoryList();
    }


    void InitialiseInventoryList()
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            inventory[i] = -1;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if(isCooking)
        {
            cookTime -= Time.deltaTime;

            

            if (cookTime <= 0f)
            {
                Debug.Log("You coooked a RECIPE!!!!!!!!!");
                isCooking = false;
            
                recipe.product = product;
                product.SetActive(true);
                if (fill) fill.fillAmount = 0f;
                EvaluateInventory();

            }
            else
            {
                if (fill) fill.fillAmount = Unity.Mathematics.math.remap(0f, (float)recipe.cookTime, 0f, 1f, recipe.cookTime - cookTime); 
            }
        }
        
    }

 

    public void EvaluateInventory()
    {
        Debug.Log("Chef: Evaluating inventory.");

        int numRecipeItemsFound = 0;

        // if inventory contains burger recipe, show burger button
        for (int i = 0; i < recipe.recipeIngredients.Length; i++)
        {

            for (int j = 0; j < inventory.Length; j++)
            {
                if (recipe.recipeIngredients[i] == inventory[j])
                {
                    numRecipeItemsFound++;
                    break;
                }
            }
        }

        Debug.Log("Chef has evaluated inventory.  Found " + numRecipeItemsFound + " matching inventory items.");

        if (numRecipeItemsFound == recipe.recipeIngredients.Length)
        {
            cookMealButton.SetActive(true);
            
        }
        else
        {
            Debug.Log("Chef doesn't have all the ingredients needed.");
        }


    }




  


    // press button to make hamburger
    public void MakeMeal()
    {

        Debug.Log("Chef: Making a hamburger.");

        int numRecipeItemsFound = 0;

        // if inventory contains burger recipe, make the burger
        for (int i = 0; i < recipe.recipeIngredients.Length; i++)
        {


            for (int j = 0; j < inventory.Length; j++)
            {
                if (recipe.recipeIngredients[i] == inventory[j])
                {
                    numRecipeItemsFound++;
                    inventory[j] = -1;
                    break;
                }
            }
        }
        

        Debug.Log("Chef found " + numRecipeItemsFound + " matching inventory items.");

        if (numRecipeItemsFound == recipe.recipeIngredients.Length)
        {
            Debug.Log("Chef made a hamburger.");
            // chef makes hamburger and put it on the table
            cookMealButton.SetActive(false);

            isCooking = true;
          
            cookTime = recipe.cookTime;
          
           
        }
        else
        {
            Debug.Log("Chef failed to make hamburger.");
        }


    }


    public void CollectMeal()
    {
        product.SetActive(false);
    }

   
}
